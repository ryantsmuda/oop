<?php
 require_once ('animal.php');
 require_once ('frog.php');
 require_once ('ape.php');
 
$sheep = new Animal("shaun");

echo "Name : " . $sheep->name; // "shaun"
echo "<br>Legs : " . $sheep->legs; // 4
echo "<br>Cold Blooded : " . $sheep->cold_blooded; // "no"
echo "<br>";

$kodok = new Frog("buduk");
echo "<br>Name : " . $kodok->name; // "shaun"
echo "<br>Legs : " . $kodok->legs; // 4
echo "<br>Cold Blooded : " . $kodok->cold_blooded; // "no"
echo "<br>";
$kodok->jump() ; // "hop hop"
echo "<br>";

$sungokong = new Ape("kera sakti");
echo "<br>Name : " . $sungokong->name; // "shaun"
echo "<br>Legs : " . $sungokong->legs; // 4
echo "<br>Cold Blooded : " . $sungokong->cold_blooded; // "no"
echo "<br>";
$sungokong->yell() ; // "hop hop"

?>